package de.md.cicd.samples.customerinfo;

import static org.junit.Assert.*;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Ignore;

import org.mockserver.client.server.MockServerClient;
import org.mockserver.junit.MockServerRule;
import org.mockserver.model.Header;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

public class CustomerInfoProviderTest {

	@Rule
	public MockServerRule mockServerRule = new MockServerRule(this, 20000);

	private MockServerClient mockServerClient;

	private String localBaseURL = "http://localhost:20000";
	
	public static String loadData(String fileName) throws Exception {
		return String.join(" ", Files.readAllLines(Paths.get(fileName)));
	}

	@Before
	public void setUp() throws Exception {		
	}

	@After
	public void tearDown() {
		mockServerClient.reset();
	}

//	@Ignore
	@Test
	public void testCustomerInfoProviderMr() {
		try {
		  String customerId = "MC.10000";
		  
		  mockServerClient.when(
		    HttpRequest.request(localBaseURL).withPath("/parties/" + customerId)).
		         respond(HttpResponse.response().withBody(loadData("./src/test/resources/MrResponse.json")).
		        		 withHeader(new Header("Content-Type", "application/json")).
		        		 withStatusCode(200));
			
		  CustomerInfoProvider provider = new CustomerInfoProvider();
		  CustomerInfo info = provider.getCustomerInfo(customerId, localBaseURL);
		  assertEquals("MR", info.getSalutation());
		  // Der Test dient als Beispiel fuer Quarantaene Test, daher wird es absichtlich mit falschen Wert verglichen.
//          assertEquals("Denton", info.getName());
		  assertEquals("Donald Trump", info.getName());
		} catch (Exception ex) {
			ex.printStackTrace();
			fail("Exception ");
		}
	}

//	@Ignore
	@Test
	public void testCustomerInfoProviderCustomerNotExists() {
		try {
		  String customerId = "MC.9999999";
		  
		  mockServerClient.when(
		    HttpRequest.request(localBaseURL).withPath("/parties/" + customerId)).
		         respond(HttpResponse.response().withHeader(new Header("Content-Type", "application/json")).
		        		 withStatusCode(204));
		  CustomerInfoProvider provider = new CustomerInfoProvider();
		  CustomerInfo info = provider.getCustomerInfo(customerId, localBaseURL);
		  fail("Exception has to be thrown");
		} catch (Exception ex) {
		}
	}

//	@Ignore
	@Test
	public void testCustomerInfoProviderCustomerError() {
		try {
		  String customerId = "MC.Trump";
		  
		  mockServerClient.when(
		    HttpRequest.request(localBaseURL).withPath("/parties/" + customerId)).
		         respond(HttpResponse.response().withHeader(new Header("Content-Type", "application/json")).
		        		 withStatusCode(400));
		  CustomerInfoProvider provider = new CustomerInfoProvider();
		  CustomerInfo info = provider.getCustomerInfo(customerId, localBaseURL);
		  fail("Exception has to be thrown");
		} catch (Exception ex) {
		}
	}
	
}
