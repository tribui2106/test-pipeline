package de.md.cicd.samples.customerinfo;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Ignore;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.junit.MockServerRule;
import org.mockserver.model.Header;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

public class CustomerInfoApplicationTest {
	@Rule
	public MockServerRule mockServerRule = new MockServerRule(this, 20000);

	private MockServerClient mockServerClient;

	private String localBaseURL = "http://localhost:20000";

	@After
	public void tearDown() {
		mockServerClient.reset();
	}

	@Test
	public void testCustomerInfoApplicationNoParam() {
		try {
			CustomerInfoApplication app = new CustomerInfoApplication();
			app.run(new String[0]);
			fail("Exception has to be thrown");
		} catch (Exception ex) {
		}
	}
	@Test
	public void testCustomerInfoApplicationInvalidParam() {
		try {
			CustomerInfoApplication app = new CustomerInfoApplication();
			app.baseURL = localBaseURL;
			app.run("MC.AAAAA");
		} catch (Exception ex) {
			fail("Exception");		
		}
	}
	@Test
	public void testCustomerInfoApplication() {
		try {
			String customerId = "MC.3100000";

			mockServerClient.when(
					HttpRequest.request(localBaseURL).withPath(
							"/parties/" + customerId)).respond(
					HttpResponse
							.response()
							.withHeader(
									new Header("Content-Type",
											"application/json"))
							.withBody(CustomerInfoProviderTest.loadData("./src/test/resources/MrResponse.json"))
							.withStatusCode(200));
			String args[] = {customerId, "--spring.profiles.active=local"};
			CustomerInfoApplication app = new CustomerInfoApplication();
			app.baseURL = localBaseURL;
			app.queryCustomer(customerId);			
		} catch (Exception ex) {
			fail("Exception");
		}		
	}
}
