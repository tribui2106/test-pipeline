package de.md.cicd.samples.customerinfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
/**
 * Die Klasse <code>CustomerInfoApplication</code> ist ist Haupt-Klasse der Spring-Anwendung und
 * implementiert die Spring-Methode zum Starten einer Apllication.  
 */
@SpringBootApplication
@RestController
public class CustomerInfoApplication extends SpringBootServletInitializer {
	/**
	 * Das Objekt f&uuml:r das Logging.
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(CustomerInfoApplication.class);

	@Value("${party-service}")
	protected String baseURL;

	public static void main(String[] args) {
		SpringApplication.run(CustomerInfoApplication.class, args);
	}
    @GetMapping(value = "/customerinfo", produces = { MediaType.TEXT_PLAIN_VALUE})
    public String queryCustomer(@RequestParam(value="customer") String customer) {
    	CustomerInfo info = null;
    	StringBuilder result = new StringBuilder();
    	try {
		  info = new CustomerInfoProvider().getCustomerInfo(customer, baseURL);
		  result.append(info.getSalutation() + " " + info.getName());
		  LOG.info("Salut {} Name {}", info.getSalutation(), info.getName());
    	} catch (Exception ex) {
    	  LOG.error("Exception ", ex);
    	  result.append(ex.getMessage());	
    	}
    	return result.toString();
    }	

	public void run(String... args) throws CustomerInfoException {
		LOG.info("Application CustomerInfoApplication started ");
		if (args.length <= 0) {
			LOG.error("Parameter of CustomerInfoApplication missed");
			throw new CustomerInfoException("Parameter missed");
		} else {
		  String  userInfo = queryCustomer(args[0]);
			LOG.info("Result {}", userInfo);
		}
	}

}
