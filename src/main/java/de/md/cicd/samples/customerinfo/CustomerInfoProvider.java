package de.md.cicd.samples.customerinfo;

import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
/**
 * Die Klasse <code>CustomerInfoProvider</code> enth&auml;lt Methode zur Ermittlung der Kundendaten &uuml;ber 
 * die party-service. Die Methode <code>getCustomerInfo</code> ist die Haupt-Methode und 
 * liefert die Kundendaten zu einer Kundennummer.
 * 
 */
@Component
public class CustomerInfoProvider {

	private static final Logger LOG = LoggerFactory.getLogger(CustomerInfoProvider.class);
	/**
	 * Ruft per REST-Request die party-service auf und liefert einer Response-Objekt. 
	 * 
	 * @param customerId Die Kundennummer
	 * @param baseURL Die URL des party-service
	 * @return
	 * Ein <code>Response</code>-Objekt des REST-Aufrufs.
	 * @throws CustomerInfoException Falls der Kunden existiert nicht oder die Kundennummer ist 
	 * fehlerhaft oder der REST-Aufruf ist fehlerhaft.  
	 */
	protected Response getResponse(String customerId, String baseURL) throws CustomerInfoException {
		try {
			  Response response = given().log().all().contentType(ContentType.JSON).when()
				 	  .get(baseURL + "/parties/" + customerId);
			  response.then().log().all();
			  if (response.statusCode() == 204) {
				  throw new CustomerInfoException("Kunden existiert nicht");
			  } else if (response.statusCode() == 200) {
				  return response;
			  } else {
				  throw new CustomerInfoException("Error " + response.then().toString());
			  }
		} catch (Exception ex) {
			throw new CustomerInfoException(ex);			
		}
	}
	
	/**
	 * Liefert ein CustomerInfo-Objekt mit den Daten.   
	 * @param customerId Die Kundennummer
	 * @param baseURL Die URL des party-Service.
	 * @return
	 * Ein <code>CustomerInfo</code>-Objekt mit den Kundendaten 
	 * @throws CustomerInfoException Falls der Kunde existiert nicht oder die Kundennummer ist fehlerhaft.
	 */
	public CustomerInfo getCustomerInfo(String customerId, String baseURL) throws CustomerInfoException {
		LOG.info("getCustomerInfo started");
		try {
		  Response response = getResponse(customerId, baseURL);
		  CustomerInfo customerInfo = new CustomerInfo();
		  customerInfo.setSalutation(response.then().extract().path("salutation").toString());
		  customerInfo.setName(response.then().extract().path("partyLabel").toString());
		  return customerInfo;
		} catch (Exception ex) {
			throw new CustomerInfoException(ex);
		}
	}
}
