package de.md.cicd.samples.customerinfo;

public class CustomerInfo {
	private String salutation;

	private String name;
	
	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
