package de.md.cicd.samples.customerinfo;

/**
 * Exception-Klasse der Anwendung. 
 */
public class CustomerInfoException extends Exception {

	private static final long serialVersionUID = 7984435654050756401L;

	public CustomerInfoException(Throwable ex) {
		super(ex);
	}
	public CustomerInfoException(String message) {
		super(message);
	}
}
